#include <QApplication>
#include <QSurfaceFormat>
#include <QFile>

#include "cartesian.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QSurfaceFormat format;
	format.setDepthBufferSize(24);
	QSurfaceFormat::setDefaultFormat(format);

	app.setApplicationName("libreplot");
	app.setApplicationVersion("0.1");

	Cartesian widget;
	widget.show();

	return app.exec();
}
