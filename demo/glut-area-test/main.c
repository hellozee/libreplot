#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <libreplot/libreplot.h>

lp_area *area = NULL;
lp_line *line = NULL;
lp_circle *circle = NULL;
lp_text *text = NULL;

int init_resources() {

	lp_init();

	area = lp_area_gen();

	lp_area_2float(area, LP_AREA_DPI, 100, 100);

	line = lp_line_gen();
	lp_line_2float(line, LP_LINE_START, 0, 0);
	lp_line_2float(line, LP_LINE_END, +INFINITY, +INFINITY);

	circle = lp_circle_gen();
	lp_circle_float(circle, LP_CIRCLE_RADIUS, 0.5);
	lp_circle_2float(circle, LP_CIRCLE_CENTER, 0, 0);
	lp_circle_bool(circle, LP_CIRCLE_FILL_SHOW, true);
	lp_circle_bool(circle, LP_CIRCLE_LINE_SHOW, true);
	lp_circle_float(circle, LP_CIRCLE_LINE_WIDTH, 10);
	lp_circle_4float(circle, LP_CIRCLE_FILL_COLOR, 1, 0, 0, 1);

	text = lp_text_gen();
	lp_text_pointer(text, LP_TEXT_STRING, "Hello world!");
	lp_text_2float(text, LP_TEXT_POSITION, -.5, .7);
	lp_text_float(text, LP_TEXT_HEIGHT, 40);
	lp_text_4float(text, LP_TEXT_COLOR, 1, 0, 0, 1);

	return 1;
}

void window_resize(int width, int height)
{
	//width = height = width < height ? width : height;

	glViewport(0, 0, width, height);
	lp_area_4uint(area, LP_AREA_SURFACE, 0, 0, width, height);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);

	lp_area_draw_start(area);
	lp_area_draw_line(area, line);
	lp_area_draw_circle(area, circle);
	lp_area_draw_text(area, text);
	lp_area_draw_end(area);

	glutSwapBuffers();
}

void free_resources() {
	lp_area_del(area);
	lp_line_del(line);
	lp_circle_del(circle);
}

int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutCreateWindow("libreplot");

	lp_enum glew_status = glewInit();

	if (GLEW_OK != glew_status) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "No support for OpenGL 2.0 found\n");
		return 1;
	}

	if (init_resources()) {
		glutReshapeFunc(window_resize);
		glutDisplayFunc(display);
		glutMainLoop();
	}

	free_resources();
	return 0;
}
