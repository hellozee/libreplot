libreplot
=========

Data Visualizing using GPU via OpenGL3+ (or GLES2+ [1]), written in C.

Born out of the need to visualize data use GPU for Box0 project.
It does not abstract out GPU (you still have access to it) and provide a thin layer
 for plotting data.

[1] OpenGL Embedded System - such as Android

Dependencies
============

* libepoxy (recommended, optional)
* Freetype2 (text rendering)
* Harfbuzz (text shaping)
* Fontconfig (fonts database, [1])
* libpng (PNG file as texture)
* PThread (or pthread-win32)

(Only for building demo)
* GLUT
* GLEW
* Qt5

(indirect: see Angle project, for OpenGL on Microsoft Windows)

[1] for Android build, Fontconfig is not used [2]. Instead font path is hardcoded to system available font (expected).
[2] It take alot of time to build cache, which increase startup time (to 20s)

Installation
============

libreplot use cmake build system.
$ cd <libreplot-source-code>
$ mkdir build; cd build
$ cmake ..
$ make
$ make install

Known to compile with GCC and Clang

Configuration
=============

cmake:
	BUILD_SHARED_LIBS - build shared library (Boolean)
	USE_EPOXY - if OFF, libexpoxy is not used (Boolean)
	USE_GLES2 - link to GLESv2 directly (Boolean)
	USE_GLES3 - link to GLESv3 directly (Boolean)
	USE_GL3 - link to GLv3 directly (Boolean)
	USE_GL4 - link to GLv4 directly (Boolean)

	BUILD_DEMO - Build demo programs [Default: ON] (Boolean)
	ENABLE_GLUT - build demo that require GLUT [Default: ON] (Boolean)
	ENABLE_GLEW - build demo that require on GLEW [Default: ON] (Boolean)
	ENABLE_QT5 - build demo that require on Qt5 [Default: ON] (Boolean)

Usage
=====

see demo/

Known issues
============

Many, still in development

Name
====

pronounced as /libre  plot/

libreplot = lib replot
 library naming convention for UNIX is "lib<name>.so"
 and "replot" because there were already alot of plotting library [1]

[1] but could not fulfil our [ambituous] demand

libreplot = libre plot
 libre = freedom (hence the free/libre licence)
 plot = does plotting work

Getting help
============

get on to #box0 on IRC freenode
and report bug on https://gitlab.com/madresistor/libreplot

Getting involved
================

get on to #box0 on IRC freenode
and report bug on https://gitlab.com/madresistor/libreplot

Licence
=======

GNU/GPLv3 or later (see COPYING)

Credits and references
======================

https://en.wikibooks.org/wiki/OpenGL_Programming
 (without the tutorials, it would have not be possible)

https://www.madresistor.com/

Maintainer
==========

Kuldeep Singh Dhaka <kuldeep@madresistor.com>
