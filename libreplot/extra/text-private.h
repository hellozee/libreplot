/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_TEXT_PRIVATE_H
#define LIBREPLOT_EXTRA_TEXT_PRIVATE_H

#include "common.h"
#include "text.h"
#include <hb.h>
#include "font-private.h"
#include "obj-manager-private.h"

__BEGIN_DECLS

struct lp_text {
	lp_obj_manager obj_manager;

	lp_font *font;
	uint8_t *string;
	lp_vec2f position;
	lp_align align;
	float rotate;
	float height;
	lp_color4f color;
};

__END_DECLS

#endif
