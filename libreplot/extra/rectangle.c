/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "rectangle-private.h"

/* default value of rect */
static const lp_rectangle DEFAULT = {
	.line = {
		.color = LP_COLOR_BLACK,
		.width = 1,
		.show = true
	},

	.fill = {
		.color = LP_COLOR_BLACK,
		.show = false
	},

	.geom = {0, 0, 1, 1}
};

lp_rectangle *lp_rectangle_gen(void)
{
	lp_rectangle *rect = malloc(sizeof(*rect));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(rect, NULL);
	*rect = DEFAULT;
	lp_obj_manager_init(&rect->obj_manager);
	return rect;
}

OBJ_REF_FUNC(rectangle)

void lp_rectangle_del(lp_rectangle *rect)
{
	LOG_ASSERT_RET_ON_NULL_ARG(rect);
	if (lp_obj_manager_fini(&rect->obj_manager)) {
		free(rect);
	}
}

void lp_rectangle_bool(lp_rectangle *rect, lp_rectangle_part part, bool arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(rect);

	switch (part) {
	case LP_RECTANGLE_LINE_SHOW:
		rect->line.show = arg0;
	break;
	case LP_RECTANGLE_FILL_SHOW:
		rect->fill.show = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_rectangle_float(lp_rectangle *rect, lp_rectangle_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(rect);

	switch (part) {
	case LP_RECTANGLE_LINE_WIDTH:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
		rect->line.width = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_rectangle_4float(lp_rectangle *rect, lp_rectangle_part part,
			float arg0, float arg1, float arg2, float arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(rect);

	lp_color4f color = {arg0, arg1, arg2, arg3};

	switch (part) {
	case LP_RECTANGLE_FILL_COLOR:
		rect->fill.color = color;
	break;
	case LP_RECTANGLE_LINE_COLOR:
		rect->line.color = color;
	break;
	case LP_RECTANGLE_GEOMETRY:
		rect->geom.x = arg0;
		rect->geom.y = arg1;
		rect->geom.w = arg2;
		rect->geom.h = arg3;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}
