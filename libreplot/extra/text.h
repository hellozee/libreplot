/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_TEXT_H
#define LIBREPLOT_EXTRA_TEXT_H

#include "../common.h"
#include "../extra/font.h"

__BEGIN_DECLS

/* FIXME: rotation not implemeted yet! */

enum lp_align {
	LP_ALIGN_RIGHT = 0x00 << 0,
	LP_ALIGN_LEFT = 0x01 << 0,
	LP_ALIGN_HCENTER = 0x02 << 0,

	LP_ALIGN_TOP = 0x00 << 2,
	LP_ALIGN_BOTTOM = 0x01 << 2,
	LP_ALIGN_VCENTER = 0x02 << 2,

	/* mask */
	LP_ALIGN_MASK_HDIR = 0x01 << 0,
	LP_ALIGN_MASK_HCENTER = 0x02 << 0,
	LP_ALIGN_MASK_VDIR = 0x01 << 2,
	LP_ALIGN_MASK_VCENTER = 0x02 << 2,
};

typedef enum lp_align lp_align;

typedef struct lp_text lp_text;

LP_API lp_text *lp_text_gen(void);
LP_API void lp_text_ref(lp_text *text);
LP_API void lp_text_del(lp_text *text);

enum lp_text_part {
	/* enum: lp_text_align */
	LP_TEXT_ALIGN = 0,

	/* 2float: x and y */
	LP_TEXT_POSITION = 10,

	/* float */
	LP_TEXT_HEIGHT = 20,
	LP_TEXT_ROTATE = 21,

	/* 4float: rgba */
	LP_TEXT_COLOR = 30,

	/* pointer: lp_font */
	LP_TEXT_FONT = 40,

	/* pointer: string */
	LP_TEXT_STRING = 50
};

typedef enum lp_text_part lp_text_part;

LP_API void lp_text_float(lp_text *text, lp_text_part part, float arg0);
LP_API void lp_text_2float(lp_text *text, lp_text_part part, float arg0, float arg1);
LP_API void lp_text_4float(lp_text *text, lp_text_part part, float arg0, float arg1,
								float arg2, float arg3);
LP_API void lp_text_enum(lp_text *text, lp_text_part part, lp_enum arg0);
LP_API void lp_text_pointer(lp_text *text, lp_text_part part, void *arg0);

__END_DECLS

#endif
