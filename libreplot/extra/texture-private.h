/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_TEXTURE_PRIVATE_H
#define LIBREPLOT_EXTRA_TEXTURE_PRIVATE_H

#include "../common.h"
#include "texture.h"
#include "obj-manager-private.h"

__BEGIN_DECLS

struct lp_texture {
	lp_obj_manager obj_manager;

	/* if the texture can be used.
	 *  if id=0, then the texture is not valid */
	GLuint id;

	/* opengl specific */
	GLint internalFormat;
	GLsizei width;
	GLsizei height;
	GLenum format;
	GLenum type;
	GLvoid *data;

	lp_free_cb free_cb;
};

__END_DECLS

#endif
