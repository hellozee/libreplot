/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "texture-private.h"
#include "font-private.h"
#include <hb.h>
#include <stdio.h>
#include <png.h>

/* PNG header check */
#define PNG_SIG_BYTES 8

static lp_texture *gen(GLuint id, GLint internalFormat,
	GLsizei width, GLsizei height, GLenum format,
	GLenum type, GLvoid *data, lp_free_cb free_cb)
{
	lp_texture *texture = malloc(sizeof(*texture));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(texture, NULL);
	lp_obj_manager_init(&texture->obj_manager);

	texture->id = id;
	texture->internalFormat = internalFormat;
	texture->width = width;
	texture->height = height;
	texture->format = format;
	texture->type = type;
	texture->data = data;
	texture->free_cb = free_cb;

	return texture;
}
/* + */
#define PLUS_WIDTH 12
#define PLUS_HEIGHT 12
#define PLUS_FORMAT GL_ALPHA
#define PLUS_TYPE GL_UNSIGNED_BYTE
static const uint8_t PLUS[PLUS_WIDTH * PLUS_HEIGHT] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
};

lp_texture *lp_texture_gen(void)
{
	return lp_texture_gen_direct(PLUS_FORMAT, PLUS_WIDTH, PLUS_HEIGHT,
		PLUS_FORMAT, PLUS_TYPE, (GLvoid *) PLUS, NULL);
}

/**
 * Build a texture out of this data and
 * @param ID of the generated texture (0 on failure)
 */
static GLuint build_texture(GLint internalFormat,
	GLsizei width, GLsizei height, GLenum format,
	GLenum type, GLvoid *data)
{
	GLuint id;

	LOG_GL_ERROR(glGenTextures(1, &id));
	LOG_ASSERT_RET_ON_FAIL(id > 0, 0);
	LOG_GL_ERROR(glBindTexture(GL_TEXTURE_2D, id));
	LOG_GL_ERROR(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));
	LOG_GL_ERROR(glTexImage2D(GL_TEXTURE_2D, 0, internalFormat,
		width, height, 0, format, type, data));

	LOG_GL_ERROR(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	LOG_GL_ERROR(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	LOG_GL_ERROR(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
	LOG_GL_ERROR(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	LOG_GL_ERROR(glBindTexture(GL_TEXTURE_2D, 0));

	return id;
}

lp_texture *lp_texture_gen_direct(GLint internalFormat,
	GLsizei width, GLsizei height, GLenum format,
	GLenum type, GLvoid *data, lp_free_cb free_cb)
{
	LOG_ASSERT_RET_ON_NULL_ARG(data, NULL);
	GLuint id = build_texture(internalFormat, width, height, format, type, data);
	return gen(id, internalFormat, width, height, format, type, data, free_cb);
}

void lp_texture_renew(lp_texture *texture, unsigned tag)
{
	LOG_ASSERT_RET_ON_NULL_ARG(texture);

	if (lp_obj_manager_renew(&texture->obj_manager, tag)) {
		/* rebuild the texture */
		texture->id = build_texture(texture->internalFormat, texture->width,
			texture->height, texture->format, texture->type, texture->data);
	}
}

OBJ_REF_FUNC(texture)

void lp_texture_del(lp_texture *texture)
{
	LOG_ASSERT_RET_ON_NULL_ARG(texture);

	if (lp_obj_manager_fini(&texture->obj_manager)) {
		LOG_GL_ERROR(glDeleteTextures(1, &texture->id));

		/* free the texture data */
		if (texture->free_cb != NULL) {
			texture->free_cb(texture->data);
		}

		free(texture);
	}
}

/**
 * @param texture_data Texture to copy to
 * @param texture_width @a texture_data columns
 * @param texture_height @a texture_data rows
 * @param bitmap_data Texture to copy to
 * @param bitmap_width @a bitmap_data columns
 * @param bitmap_height @a bitmap_data rows
 * Copying is done in row major style.
 *  Also Freetype and OpenGL data is row major.
 */
static void copy_bitmap_to_texture(
	uint8_t *texture_data, unsigned texture_width, unsigned texture_height,
	uint8_t *bitmap_data, unsigned bitmap_width, unsigned bitmap_height,
	unsigned off_x, unsigned off_y)
{
	unsigned i;

	size_t max_dest_off = texture_width * texture_height;

	for (i = 0; i < bitmap_height; i++) {
		size_t src_off = i * bitmap_width;
		size_t dest_off = (texture_width * (off_y + i)) + off_x;

		LOG_ASSERT_RET_ON_FAIL((dest_off + bitmap_width) <= max_dest_off);
		memcpy(&texture_data[dest_off], &bitmap_data[src_off], bitmap_width);
	}
}

lp_texture *lp_texture_gen_text(uint8_t *str, float char_height,
		lp_font *font, lp_vec2f *dpi)
{
	hb_buffer_t *hb_buffer;

	LOG_ASSERT_RET_ON_NULL_ARG(str, NULL);
	LOG_ASSERT_RET_ON_NULL_ARG(font, NULL);
	LOG_ASSERT_RET_ON_NULL_ARG(dpi, NULL);
	LOG_ASSERT_RET_ON_FAIL(char_height > 0, NULL);

	if (FT_Set_Char_Size(font->ft_face, 0, char_height * 64, dpi->x, dpi->y)) {
		LOG_WARN("FT_Set_Char_Size failed");
		return NULL;
	}

	hb_buffer = hb_buffer_create();
	LOG_ASSERT_RET_ON_NULL(hb_buffer, NULL);

	hb_buffer_add_utf8(hb_buffer, (const char *) str, -1, 0, -1);
	hb_buffer_guess_segment_properties(hb_buffer);
	hb_shape(font->hb_font, hb_buffer, NULL, 0);

	/* Create a texture that will be used to hold one "glyph" */
	GLuint tex;
	LOG_GL_ERROR(glGenTextures(1, &tex));
	LOG_GL_ERROR(glBindTexture(GL_TEXTURE_2D, tex));

	/* We require 1 byte alignment when uploading texture data */
	LOG_GL_ERROR(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

	/* Clamping to edges is important to prevent artifacts when scaling */
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

	/* Get glyph information and positions out of the buffer. */
	unsigned int i, len = hb_buffer_get_length(hb_buffer);
	hb_glyph_info_t *info = hb_buffer_get_glyph_infos(hb_buffer, NULL);

	FT_GlyphSlot g = font->ft_face->glyph;
	unsigned int width_26p6 = 0, width, height = 0;

	/* calculate the height/width of final texture */
	for (i = 0; i < len; i++) {
		/* Try to load and render the character */
		if (FT_Load_Glyph(font->ft_face, info[i].codepoint, FT_LOAD_RENDER)) {
			LOG_WARN("FT_Load_Glyph failed for code point 0x%x", info[i].codepoint);
			continue;
		}

		/* TODO: assuming horizontal string */
		width_26p6 += g->advance.x;
		height = MAX(height, g->bitmap.rows);
	}

	width = ceil(width_26p6 / 64.0);

	/* Copy the glyph's to a common buffer */
	uint8_t *data = calloc(1, width * height);
	signed long x_26p6 = 0, y_26p6 = 0;

	/* Loop through all codepoint and draw them */
	for (i = 0; i < len; i++) {
		/* Try to load and render the character */
		if (FT_Load_Glyph(font->ft_face, info[i].codepoint, FT_LOAD_RENDER)) {
			LOG_WARN("FT_Load_Glyph failed for code point 0x%x", info[i].codepoint);
			continue;
		}

		/* Upload the "bitmap", which contains an 8-bit grayscale image,
		 *  as an alpha texture */
		unsigned offset_x = ceilf((x_26p6) / 64.0);
		unsigned offset_y = ceilf((y_26p6) / 64.0);
		copy_bitmap_to_texture(data, width, height,
			g->bitmap.buffer, g->bitmap.width, g->bitmap.rows,
			offset_x, offset_y);

		/* Advance the cursor to the start of the next character */
		x_26p6 += g->advance.x;
		y_26p6 += g->advance.y;
	}

	return lp_texture_gen_direct(GL_ALPHA, width, height, GL_ALPHA,
		GL_UNSIGNED_BYTE, data, free);
}

struct libpng_proxy_callback {
	lp_io_cb read_cb;
	void *user_data;
};

/**
 * since the format (of callback) which libpng have is not compatible with
 *  lp_io_callback, this glue code does the routing stuff from libpng to
 *  the application code read callback.
 */
static void proxy_libpng_callback(png_structp png_ptr, png_bytep dest,
	png_size_t read_bytes)
{
	struct libpng_proxy_callback *proxy = png_get_io_ptr(png_ptr);
	LOG_ASSERT_RET_ON_NULL(proxy);
	proxy->read_cb(proxy->user_data, dest, read_bytes);
}

/**
 * Open a png file via FILE object or custom read.
 * @param read_cb if NULL, @a user_data is assumed as FILE
 * @param user_data argument to pass if @a read_cb is not NULL or FILE*
 */
static lp_texture *gen_png_read(lp_io_cb read_cb, void *user_data)
{
	if (read_cb == NULL) {
		/* user_data cannot be NULL if read_cb is NULL, because
		 *  user_data is of type FILE* if read_cb is NULL */
		LOG_ASSERT_RET_ON_NULL_ARG(user_data, NULL);
	}

	png_info *info_ptr = NULL;
	png_struct *png_ptr = NULL;

	/* read the signature */
	uint8_t sign[PNG_SIG_BYTES];
	size_t sign_readed;
	if (read_cb != NULL) {
		/* user custom function */
		sign_readed = read_cb(user_data, sign, PNG_SIG_BYTES);
	} else {
		/* standard io */
		sign_readed = fread(sign, 1, PNG_SIG_BYTES, user_data);
	}

	/* verify that we have readed */
	LOG_ASSERT_RET_ON_FAIL(sign_readed == PNG_SIG_BYTES, NULL);

	/* verify the signature */
	if (png_sig_cmp(sign, 0, PNG_SIG_BYTES)) {
		LOG_WARN("Not PNG, Signature failed!");
		return NULL;
	}

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		LOG_WARN("png_create_read_struct failed!");
		goto error;
	}

	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		LOG_WARN("png_create_info_struct failed!");
		goto error;
	}

	if (setjmp(png_jmpbuf(png_ptr))) {
		LOG_WARN("something failed");
		goto error;
	}

	/* custom glue callback */
	struct libpng_proxy_callback proxy = {read_cb, user_data};

	if (read_cb != NULL) {
		/* custom read callback */
		png_set_read_fn(png_ptr, &proxy, proxy_libpng_callback);
	} else {
		/* user_data is FILE* */
		png_init_io(png_ptr, user_data);
	}

	/* we have readed some of the signature
	 *  previously to detect that is it really PNG
	 */
	png_set_sig_bytes(png_ptr, PNG_SIG_BYTES);

	/* start reading */
	png_read_info(png_ptr, info_ptr);

	png_uint_32 width, height;
	int bit_depth, color_type;
	LOG_WARN_IF_FAIL(png_get_IHDR(png_ptr, info_ptr, &width, &height,
		&bit_depth, &color_type, NULL, NULL, NULL) == 1);

	/* the libpng equivalent format (after some tweak) for OpenGL */
	GLenum texture_format;

	switch(color_type) {
	case PNG_COLOR_TYPE_GRAY:
		if (bit_depth < 8) {
			png_set_expand_gray_1_2_4_to_8(png_ptr);
		}
		texture_format = GL_LUMINANCE;
	break;
	case PNG_COLOR_TYPE_GRAY_ALPHA:
		texture_format = GL_LUMINANCE_ALPHA;
	break;
	case PNG_COLOR_TYPE_RGB:
		texture_format = GL_RGB;
	break;
	case PNG_COLOR_TYPE_RGB_ALPHA:
		texture_format = GL_RGBA;
	break;
	case PNG_COLOR_TYPE_PALETTE:
		texture_format = GL_RGBA;
		png_set_palette_to_rgb(png_ptr);
	break;
	default:
		LOG_WARN("color_type (%i) not supported", color_type);
		goto error;
	}

	/* OpenGL ES only support 8bit */
	if (bit_depth == 16) {
		png_set_strip_16(png_ptr);
	}

	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) {
		png_set_tRNS_to_alpha(png_ptr);
	}

	png_size_t cols = png_get_rowbytes(png_ptr, info_ptr);
	png_byte **rows_mapper = malloc(sizeof(*rows_mapper) * height);
	png_byte *bitmap_data = malloc(sizeof(*bitmap_data) * cols * height);
	unsigned i;

	/*
	 * PNG is ordered from top to bottom BUT
	 * and OpenGL images are ordered from bottom to top
	 * so, using row pointers we point to the memory
	 *  that will lead to inverted image
	 */
	for (i = 0; i < height; i++) {
		rows_mapper[height - i - 1] = &bitmap_data[i * cols];
	}

	png_read_image(png_ptr, rows_mapper);
	png_read_end(png_ptr, info_ptr);
	png_destroy_read_struct(&png_ptr, &info_ptr, 0);
	free(rows_mapper);

	return lp_texture_gen_direct(texture_format, width, height,
		texture_format, GL_UNSIGNED_BYTE, bitmap_data, free);

	error:
	png_destroy_read_struct(
		(png_ptr == NULL) ? NULL : &png_ptr,
		(info_ptr == NULL) ? NULL : &info_ptr, NULL);
	return NULL;
}

lp_texture *lp_texture_gen_png_read(lp_io_cb read_cb, void *user_data)
{
	LOG_ASSERT_RET_ON_NULL_ARG(read_cb, NULL);
	return gen_png_read(user_data, read_cb);
}

struct texture_png_read {
	void *buf;
	size_t total;
	size_t used;
};

/**
 * this is a glue code to map lp_io_cb (read) and buffer.
 * @param ref Pointer to stored information
 * @param buf Buffer to read data into
 * @param len Number of bytes to read
 */
static size_t png_buf_reader(struct texture_png_read *ref, void *buf, size_t len)
{
	size_t rem = ref->total - ref->used;
	len = MIN(len, rem);

	if (len) {
		memcpy(buf, ref->buf + ref->used, len);
		ref->used += len;
	}

	return len;
}

lp_texture *lp_texture_gen_png_mem(void *buf, size_t total_len, lp_free_cb free_cb)
{
	lp_texture *texture;
	struct texture_png_read reader = {buf, total_len, 0};

	texture = lp_texture_gen_png_read((lp_io_cb) png_buf_reader, &reader);

	/* free the buffer because we do not need now */
	if (free_cb != NULL) {
		free_cb(buf);
	}

	return texture;
}

lp_texture *lp_texture_gen_png_file(const char *filename)
{
	lp_texture *texture;
	FILE *file;

	LOG_ASSERT_RET_ON_NULL_ARG(filename, NULL);

	file = fopen(filename, "r");
	LOG_ASSERT_RET_ON_NULL(file, NULL);

	texture = gen_png_read(NULL, file);

	fclose(file);

	return texture;

	return texture;
}
