/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_TEXTURE_H
#define LIBREPLOT_EXTRA_TEXTURE_H

#include "../common.h"
#include "font.h"

__BEGIN_DECLS

typedef struct lp_texture lp_texture;

/**
 * Return a texture with little or no hassle
 */
LP_API lp_texture *lp_texture_gen(void);
LP_API void lp_texture_ref(lp_texture *texture);
LP_API void lp_texture_del(lp_texture *texture);

/**
 * Generate a Texture from [Unicode] text.
 * They are huge number of mathematical symbol (and emoji) that can be reused.
 * @param str Unicode string (utf-8)
 * @param height Height of character (in pixels)
 * @param font Font to use
 * @param dpi screen DPI
 */
LP_API lp_texture *lp_texture_gen_text(uint8_t *str, float height, lp_font *font,
			lp_vec2f *dpi);

/**
 * @param internalFormat see glTexImage2D()
 * @param width see glTexImage2D()
 * @param height see glTexImage2D()
 * @param format see glTexImage2D() (must match @a internalFormat if GLES 2.0)
 * @param type see glTexImage2D()
 * @param data see glTexImage2D() - cannot be NULL
 * @param free_cb Function to be called when done with data (with argument @a data).
 * @note @a data passed to this function is owned by texture object
 *  if @a free_cb is not NULL.
 * @note It is recommended to generate a texture with @a width = @a height.
 *   If the relation is not mantained,
 *     streching will occur while drawing the texture as marker.
 */
lp_texture *lp_texture_gen_direct(GLint internalFormat,
	GLsizei width, GLsizei height, GLenum format,
	GLenum type, GLvoid *data, lp_free_cb free_cb);

/**
 * Read data via @a read function to read file. (custom IO)
 * @param user_data User data to pass to @a read
 * @param read Read function
 * @return texture object
 */
LP_API lp_texture *lp_texture_gen_png_read(lp_io_cb read_cb, void *user_data);

/**
 * User the memory as png buffer (to read)
 * @param buf Buffer
 * @param total_len Total byte length of @a buf
 * @param free_cb function to be called to free @a buf
 *   if free is NULL, ownership to texture object is not passed
 * @return texture object
 */
LP_API lp_texture *lp_texture_gen_png_mem(void *buf, size_t total_len, lp_free_cb free_cb);

/**
 * Build png via standard io.
 * @param filename Filename
 * @return texture object
 */
LP_API lp_texture *lp_texture_gen_png_file(const char *filename);

LP_API void lp_texture_renew(lp_texture *texture, unsigned tag);

__END_DECLS

#endif
