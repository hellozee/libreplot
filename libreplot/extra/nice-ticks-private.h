/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_NICE_TICKS_H
#define LIBREPLOT_EXTRA_NICE_TICKS_H

#include "../common.h"

__BEGIN_DECLS

/* double is used to reduce change to precision loss */

struct lp_nice_ticks_input {
	double min, max;
	unsigned ticks;
};

struct lp_nice_ticks_output {
	double min, max, inc;

	/*
	 * for (x = start; x < end + (0.5 * d); x += d) {
	 *     printf("%.*f", nfrac, x);
	 * }
	 */
};

typedef struct lp_nice_ticks_input lp_nice_ticks_input;
typedef struct lp_nice_ticks_output lp_nice_ticks_output;

LP_PRIV void lp_nice_ticks(lp_nice_ticks_input *input, lp_nice_ticks_output *output);

__END_DECLS

#endif
