/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_METRIC_PREFIX_H
#define LIBREPLOT_EXTRA_METRIC_PREFIX_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_metric_prefix lp_metric_prefix;

enum lp_metric_prefix_entry {
	LP_METRIC_PREFIX_YOCTO = -24, /* y */
	LP_METRIC_PREFIX_ZEPTO = -21, /* z */
	LP_METRIC_PREFIX_ATTO = -18, /* a */
	LP_METRIC_PREFIX_FEMTO = -15, /* f */
	LP_METRIC_PREFIX_PICO = -12, /* p */
	LP_METRIC_PREFIX_NANO = -9, /* n */
	LP_METRIC_PREFIX_MICRO = -6, /* μ */
	LP_METRIC_PREFIX_MILLI = -3, /* m */
	LP_METRIC_PREFIX_CENTI = -2, /* c */
	LP_METRIC_PREFIX_DECI = -1, /* d */
	LP_METRIC_PREFIX_NONE = 0,
	LP_METRIC_PREFIX_DECA = 1, /* da */
	LP_METRIC_PREFIX_HECTO = 2, /* h */
	LP_METRIC_PREFIX_KILO = 3, /* k */
	LP_METRIC_PREFIX_MEGA = 6, /* M */
	LP_METRIC_PREFIX_GIGA = 9, /* G */
	LP_METRIC_PREFIX_TERA = 12, /* T */
	LP_METRIC_PREFIX_PETA = 15, /* P */
	LP_METRIC_PREFIX_EXA = 18, /* E */
	LP_METRIC_PREFIX_ZETTA = 21, /* Z */
	LP_METRIC_PREFIX_YOTTA = 24, /* Y */
};

typedef enum lp_metric_prefix_entry lp_metric_prefix_entry;

LP_API lp_metric_prefix *lp_metric_prefix_gen(void);
LP_API void lp_metric_prefix_ref(lp_metric_prefix *metric_prefix);
LP_API void lp_metric_prefix_del(lp_metric_prefix *metric_prefix);

LP_API void lp_metric_prefix_add(lp_metric_prefix *metric_prefix,
				const lp_metric_prefix_entry *entries, size_t len);

/* WARNING: do not modify the returned list. */
LP_API lp_metric_prefix *lp_metric_prefix_commonly_used(void);

__END_DECLS

#endif
