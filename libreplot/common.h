/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_COMMON_H
#define LIBREPLOT_COMMON_H

/** libreplot can use multiple GL header
 * LP_USE_EPOXY - use libepoxy header
 * LP_USE_GLES2 - use GLES2/ header
 * LP_USE_GLES3 - use GLES3/ header
 * LP_USE_GLES1 - use GLES/ header
 * LP_USE_GL3, LP_USE_GL4 - use GL/ header.
 * Headers are required for some part of the code
 * libreplot do not does GL typedef, it depends on other header for that
 *
 * if nothing is defined, nothing is included
 */
#if defined(LP_USE_EPOXY)
# include <epoxy/gl.h>
#elif defined(LP_USE_GLES2)
# include <GLES2/gl2.h>
#elif defined(LP_USE_GLES3)
# include <GLES3/gl3.h>
#elif defined(LP_USE_GL3) || defined(LP_USE_GL4)
# include <GL/gl.h>
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/* __BEGIN_DECLS should be used at the beginning of your declarations,
   so that C++ compilers don't mangle their names.  Use __END_DECLS at
   the end of C declarations. */
#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

/* Reference: http://gcc.gnu.org/wiki/Visibility */
#if defined(_WIN32) || defined(__CYGWIN__)
# if defined(replot_EXPORTS)
#  if defined(__GNUC__)
#   define LP_API __attribute__ ((dllexport))
#  else /* defined(__GNUC__) */
   /* Note: actually gcc seems to also supports this syntax. */
#   define LP_API __declspec(dllexport)
#  endif /* defined(__GNUC__) */
# else /* defined(replot_EXPORTS) */
#  if defined(__GNUC__)
#   define LP_API __attribute__ ((dllimport))
#  else
   /* Note: actually gcc seems to also supports this syntax. */
#   define LP_API __declspec(dllimport)
#  endif
# endif /* defined(replot_EXPORTS) */
# define LP_PRIV
#else /* defined(_WIN32) || defined(__CYGWIN__) */
# if __GNUC__ >= 4
#  define LP_API __attribute__ ((visibility ("default")))
#  define LP_PRIV  __attribute__ ((visibility ("hidden")))
# else /* __GNUC__ >= 4 */
#  define LP_API
#  define LP_PRIV
# endif /* __GNUC__ >= 4 */
#endif /* defined(_WIN32) || defined(__CYGWIN__) */

typedef int lp_enum;

typedef struct lp_vec2f { float x, y; } lp_vec2f;

typedef struct lp_vec4f { float x, y, s, t; } lp_vec4f;

typedef struct lp_color4f { float r, g, b, a; } lp_color4f;

typedef struct lp_surface { unsigned x, y, width, height; } lp_surface;

/**
 * @param buf Buffer to free
 */
typedef void (*lp_free_cb)(void *buf);

/**
 * @param user_data User data (provided by User)
 * @param buf Buffer
 * @param len Number of bytes to read/write
 * @return number of actually readed/written
 */
typedef size_t (*lp_io_cb)(void *user_data, void *buf, size_t len);

/**
 * Enable all the required facility from OpenGL.
 *  This need to be called only once when the context is created.
 */
LP_API void lp_init(void);

/**
 * Related to libepoxy (epoxy_handle_external_wglMakeCurrent()).
 *
 * https://github.com/anholt/libepoxy#windows-issues
 * The automatic per-context symbol resolution for win32 requires that epoxy
 * knows when wglMakeCurrent() is called, because wglGetProcAddress() return
 * values depend on the context's device and pixel format. If wglMakeCurrent()
 * is called from outside of epoxy (in a way that might change the device or
 * pixel format), then epoxy needs to be notified of the change using the
 * epoxy_handle_external_wglMakeCurrent() function.
 *
 * The win32 wglMakeCurrent() variants are slower than they should be,
 * because they should be caching the resolved dispatch tables instead of
 * resetting an entire thread-local dispatch table every time.
 *
 * @note This function is No operations if called from other platform other
 *  than Microsoft Windows.
 */
LP_API void lp_epoxy_handle_external_wglMakeCurrent(void);

__END_DECLS

#endif
