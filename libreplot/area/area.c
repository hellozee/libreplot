/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "area-private.h"
#include "../extra/font-private.h"
#include "../extra/text-private.h"
#include "../extra/line-private.h"
#include "../extra/circle-private.h"

static const lp_area DEFAULT = {
	.dpi = {0, 0},
	.surface = {0, 0, 0, 0},

	.program = {
		.generic = NULL,
		.circle = NULL,
		.text = NULL
	},

	.reuse = {
		.hb_buffer = NULL
	}
};

lp_area *lp_area_gen(void)
{
	lp_area *area = malloc(sizeof(*area));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(area, NULL);
	*area = DEFAULT;
	area->program.generic = lp_program_generic_gen(NULL);
	area->program.circle = lp_program_circle_gen(NULL);
	area->program.text = lp_program_text_gen(NULL);
	lp_obj_manager_init(&area->obj_manager);
	return area;
}

OBJ_REF_FUNC(area)

void lp_area_del(lp_area *area)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);

	if (lp_obj_manager_fini(&area->obj_manager)) {
		lp_program_generic_del(area->program.generic);
		lp_program_circle_del(area->program.circle);
		lp_program_text_del(area->program.text);
		free(area);
	}
}

LP_API void lp_area_4uint(lp_area *area, lp_area_part part,
			unsigned arg0, unsigned arg1, unsigned arg2, unsigned arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);

	switch (part) {
	case LP_AREA_SURFACE:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg2 > 0);
		LOG_ASSERT_RET_ON_FAIL_ARG(arg3 > 0);
		area->surface = (lp_surface){arg0, arg1, arg2, arg3};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

LP_API void lp_area_2float(lp_area *area, lp_area_part part,
				float arg0, float arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);

	switch (part) {
	case LP_AREA_DPI:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
		LOG_ASSERT_RET_ON_FAIL_ARG(arg1 > 0);
		area->dpi = (lp_vec2f){arg0, arg1};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_area_draw_start(lp_area *area)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
}

void lp_area_draw_line(lp_area *area, lp_line *line)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
	LOG_ASSERT_RET_ON_NULL_ARG(line);

	lp_vec2f translate = LP_TRANSLATE_NONE;
	lp_vec2f scale = LP_SCALE_NONE;

	lp_program_generic_draw_line(area->program.generic,
		line, &scale, &translate);
}

/**
 * Get a temporary (reusable) harfbuzz buffer
 * @param area Area object
 * @return harbuzz buffer object
 */
static hb_buffer_t *get_tmp_hb_buffer(lp_area *area)
{
	if (area->reuse.hb_buffer == NULL) {
		area->reuse.hb_buffer = hb_buffer_create();
	}

	return area->reuse.hb_buffer;
}

/*
 * ENHANCE: this function could cache the result of the resultant text.
 * EHNAHCE: this function can use perform the same work using one draw call.
 */
void lp_area_draw_text(lp_area *area, lp_text *text)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	lp_vec2f translate = LP_TRANSLATE_NONE;

	lp_program_text_draw_text(area->program.text,
		text, get_tmp_hb_buffer(area),
		&area->surface, &area->dpi, &translate);
}

void lp_area_draw_circle(lp_area *area, lp_circle *circle)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
	LOG_ASSERT_RET_ON_NULL_ARG(circle);

	lp_vec2f scale = LP_SCALE_NONE;
	lp_vec2f translate = LP_TRANSLATE_NONE;

	lp_program_circle_draw_circle(area->program.circle,
		circle, &scale, &translate);
}

void lp_area_draw_circle_arc(lp_area *area, lp_circle_arc *arc)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
	LOG_ASSERT_RET_ON_NULL_ARG(arc);
	/* TODO: implement */
}

void lp_area_draw_circle_segment(lp_area *area, lp_circle_segment *segment)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
	LOG_ASSERT_RET_ON_NULL_ARG(segment);
	/* TODO: implement */
}

void lp_area_draw_circle_sector(lp_area *area, lp_circle_sector *sector)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
	LOG_ASSERT_RET_ON_NULL_ARG(sector);
	/* TODO: implement */
}

void lp_area_draw_end(lp_area *area)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);
}

#define RENEW_AREA_PROGRAM(name)									\
	area->program.name =											\
			lp_program_ ##name ##_gen(area->program.name)

void lp_area_renew(lp_area *area, unsigned tag)
{
	LOG_ASSERT_RET_ON_NULL_ARG(area);

	if (lp_obj_manager_renew(&area->obj_manager, tag)) {
		/* regenerate different program's */
		RENEW_AREA_PROGRAM(generic);
		RENEW_AREA_PROGRAM(circle);
		RENEW_AREA_PROGRAM(text);
	}
}
