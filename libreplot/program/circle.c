/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "circle.h"
#include "../extra/circle-private.h"

static const char VERTEX_SHADER[] =
	"uniform float uniform_div_angle;"
	"attribute float attribute_index;"
	"uniform float uniform_radius;"
	"uniform vec2 uniform_center;"
	"uniform vec2 uniform_scale;"
	"uniform vec2 uniform_translate;"
	"void main(void) {"
		"vec2 pos = uniform_center;"
		"float angle = uniform_div_angle * attribute_index;"
		"pos += uniform_radius * vec2(cos(angle), sin(angle));"
		"pos = (pos * uniform_scale) + uniform_translate;"
		"gl_Position = vec4(pos, 0, 1);"
	"}";

static const char FRAGMENT_SHADER[] =
	"uniform vec4 uniform_color;"
	"void main(void) {"
		"gl_FragColor = uniform_color;"
	"}";

static const lp_program_circle DEFAULT = {
	.id = -1,
	.uniform = {
		.color = -1,
		.div_angle = -1,
		.radius = -1,
		.center = -1,
		.scale = -1,
		.translate = -1
	},
	.attribute = {
		.index = -1
	}
};

lp_program_circle *lp_program_circle_gen(lp_program_circle *program)
{
	if (program == NULL) {
		program = malloc(sizeof(*program));
		LOG_ASSERT_RET_ON_ALLOC_FAIL(program, NULL);
	}

	*program = DEFAULT;

	LOG_DEBUG("compiling circle program");
	program->id = lp_program_create(VERTEX_SHADER, FRAGMENT_SHADER);
	program->attribute.index = lp_program_attribute(program->id, "attribute_index");
	program->uniform.color = lp_program_uniform(program->id, "uniform_color");
	program->uniform.div_angle = lp_program_uniform(program->id, "uniform_div_angle");
	program->uniform.radius = lp_program_uniform(program->id, "uniform_radius");
	program->uniform.center = lp_program_uniform(program->id, "uniform_center");
	program->uniform.scale = lp_program_uniform(program->id, "uniform_scale");
	program->uniform.translate = lp_program_uniform(program->id, "uniform_translate");

	LOG_DEBUG_INT(program->id);
	LOG_DEBUG_INT(program->attribute.index);
	LOG_DEBUG_INT(program->uniform.color);
	LOG_DEBUG_INT(program->uniform.div_angle);
	LOG_DEBUG_INT(program->uniform.radius);
	LOG_DEBUG_INT(program->uniform.center);
	LOG_DEBUG_INT(program->uniform.scale);
	LOG_DEBUG_INT(program->uniform.translate);

	return program;
}

void lp_program_circle_del(lp_program_circle *program)
{
	LOG_ASSERT_RET_ON_NULL_ARG(program);

	LOG_GL_ERROR(glDeleteProgram(program->id));
	free(program);
}

static short *get_inited_circle_index_array(size_t N)
{
	unsigned short i;
	short *arr;

	/* ushort was chosen over uint because we do not expect to exceed
	 *  32K lines while drawing a circle */
	static short *index_array;
	static size_t index_len;

	/* we already have a calculated array */
	if (index_len >= N) {
		return index_array;
	}

	/* free any previous array */
	if (index_array != NULL) {
		free(index_array);
		index_array = NULL;
		index_len = 0;
	}

	/* allocate array */
	arr = malloc(sizeof(*arr) * N);
	LOG_ASSERT_RET_ON_ALLOC_FAIL(arr, NULL);

	/* initalize array */
	for (i = 0; i < N; i++) {
		arr[i] = i;
	}

	/* keep a reference */
	index_len = N;
	index_array = arr;

	/* and we have it! */
	return arr;
}

/*
 * return the number of division to perform
 */
static unsigned circle_division_general(float radius, float approx_arc)
{
	float circumf = (M_PI * 2 * radius);
	return  (unsigned) ceilf(circumf / approx_arc);
}

static unsigned circle_division(lp_circle *circle)
{
	return circle_division_general(circle->radius, circle->approx_arc);
}

void lp_program_circle_draw_circle(lp_program_circle *program,
		lp_circle *circle, lp_vec2f *scale, lp_vec2f *translate)
{
	size_t N = circle_division(circle);
	short *indexes = get_inited_circle_index_array(N);
	float div_angle = (2 * M_PI) / N;

	LOG_GL_ERROR(glUseProgram(program->id));
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, 0));

	/* common parameters */
	LOG_GL_ERROR(glUniform1f(program->uniform.radius, circle->radius));
	LOG_GL_ERROR(glUniform1f(program->uniform.div_angle, div_angle));
	LOG_GL_ERROR(glUniform2fv(program->uniform.center, 1,
		CAST_TO_FLOAT_PTR(&circle->center)));
	LOG_GL_ERROR(glUniform2fv(program->uniform.scale, 1,
		CAST_TO_FLOAT_PTR(scale)));
	LOG_GL_ERROR(glUniform2fv(program->uniform.translate, 1,
		CAST_TO_FLOAT_PTR(translate)));

	/* indexes */
	LOG_GL_ERROR(glEnableVertexAttribArray(program->attribute.index));
	LOG_GL_ERROR(glVertexAttribPointer(program->attribute.index,
			1, GL_SHORT, false, 0, indexes));

	if (circle->line.show) {
		LOG_GL_ERROR(glLineWidth(circle->line.width));
		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1,
			CAST_TO_FLOAT_PTR(&circle->line.color)));
		LOG_GL_ERROR(glDrawArrays(GL_LINE_LOOP, 0, N));
	}

	if (circle->fill.show) {
		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1,
			CAST_TO_FLOAT_PTR(&circle->fill.color)));
		LOG_GL_ERROR(glDrawArrays(GL_TRIANGLE_FAN, 0, N));
	}
}
