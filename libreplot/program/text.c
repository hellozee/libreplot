/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "text.h"
#include "../extra/text-private.h"
#include "../extra/font-private.h"

/* TODO: GL_ALPHA is deprecated in newer version. GL_RED to be used */

static const char VERTEX_SHADER[] =
	"attribute vec4 attribute_coord;"
	"varying vec2 varying_texpos;"
	"void main(void) {"
		"gl_Position = vec4(attribute_coord.xy, 0, 1);"
		"varying_texpos = attribute_coord.zw;"
	"}";

static const char FRAGMENT_SHADER[] =
	"varying vec2 varying_texpos;"
	"uniform sampler2D uniform_tex;"
	"uniform vec4 uniform_color;"
	"void main(void) {"
		"float a = texture2D(uniform_tex, varying_texpos).a;"
		"gl_FragColor = a * uniform_color;"
	"}";

static const lp_program_text DEFAULT = {
	.id = -1,
	.attribute = {
		.coord = -1
	},
	.uniform = {
		.tex = -1,
		.color = -1
	}
};

lp_program_text *lp_program_text_gen(lp_program_text *program)
{
	if (program == NULL) {
		program = malloc(sizeof(*program));
		LOG_ASSERT_RET_ON_ALLOC_FAIL(program, NULL);
	}

	*program = DEFAULT;

	LOG_DEBUG("compiling program text");
	program->id = lp_program_create(VERTEX_SHADER, FRAGMENT_SHADER);
	program->attribute.coord = lp_program_attribute(program->id, "attribute_coord");
	program->uniform.color = lp_program_uniform(program->id, "uniform_color");
	program->uniform.tex = lp_program_uniform(program->id, "uniform_tex");

	LOG_DEBUG_INT(program->id);
	LOG_DEBUG_INT(program->attribute.coord);
	LOG_DEBUG_INT(program->uniform.color);
	LOG_DEBUG_INT(program->uniform.tex);
	return program;
}

void lp_program_text_del(lp_program_text *program)
{
	LOG_ASSERT_RET_ON_NULL(program);

	LOG_GL_ERROR(glDeleteProgram(program->id));
	free(program);
}

static void translate_from_aligned_and_rotate(hb_buffer_t *hb_buffer, lp_font *font,
	lp_align align, lp_surface *surface, float rotate, lp_vec2f *translate)
{
	float sx = 2.0 / surface->width;
	float sy = 2.0 / surface->height;

	if (rotate) {
		SWAP_VALUE(sx, sy, float)
	}

	/* Get glyph information and positions out of the buffer. */
	unsigned int i, len = hb_buffer_get_length(hb_buffer);
	hb_glyph_info_t *info = hb_buffer_get_glyph_infos(hb_buffer, NULL);

	FT_GlyphSlot g = font->ft_face->glyph;

	float h = 0, w = 0;

	/* calculate the box dimentions that will be required to draw the text */
	for (i = 0; i < len; i++) {
		/* Try to load and render the character */
		if (FT_Load_Glyph(font->ft_face, info[i].codepoint, FT_LOAD_RENDER)) {
			LOG_WARN("FT_Load_Glyph failed for code point %i", info[i].codepoint);
			continue;
		}

		/* assuming text is draw horizontal */
		w += (g->advance.x / 64.) * sx;
		h = MAX(h, g->bitmap.rows * sy);
	}

	lp_vec2f translate_rotate = {0, 0};
	if (rotate) {
		SWAP_VALUE(w, h, float)
		translate_rotate.x = w;
	}

	lp_vec2f translate_align = {0, 0};
	if (align & LP_ALIGN_MASK_HCENTER) {
		translate_align.x = -1 * (w / 2);
	} else if (align & LP_ALIGN_MASK_HDIR) {
		translate_align.x = -1 * w;
	}

	if (align & LP_ALIGN_MASK_VCENTER) {
		translate_align.y = -1 * (h / 2);
	} else if (align & LP_ALIGN_MASK_VDIR) {
		translate_align.y = -1 * h;
	}

	translate->x += translate_rotate.x + translate_align.x;
	translate->y += translate_rotate.y + translate_align.y;
}

/**
 * Rotate @a coords coordinates around center using @a angle
 */
static void rotate_coords(lp_vec4f *coords, size_t len, lp_vec2f *center,
		float angle)
{
	size_t i;

	float sin = sinf(angle);
	float cos = cosf(angle);

	for (i = 0; i < len; i++) {
		float tx = coords[i].x - center->x;
		float ty = coords[i].y - center->y;
		coords[i].x = (tx * cos) - (ty * sin) + center->x;
		coords[i].y = (tx * sin) + (ty * cos) + center->y;
	}
}

static void apply_translate(lp_vec4f *coords, size_t len, lp_vec2f *translate)
{
	size_t i;

	for (i = 0; i < len; i++) {
		coords[i].x += translate->x;
		coords[i].y += translate->y;
	}
}

static void render_text(lp_program_text *program, hb_buffer_t *hb_buffer,
		lp_font *font, lp_vec2f *translate, lp_vec2f *pos, lp_surface *surface,
		float rotate)
{
	float x = pos->x, y = pos->y;
	float sx = 2.0 / surface->width;
	float sy = 2.0 / surface->height;

	if (rotate) {
		SWAP_VALUE(sx, sy, float)
	}

	/* Create a texture that will be used to hold one "glyph" */
	GLuint tex;
	LOG_GL_ERROR(glActiveTexture(GL_TEXTURE0));
	LOG_GL_ERROR(glGenTextures(1, &tex));
	LOG_GL_ERROR(glBindTexture(GL_TEXTURE_2D, tex));
	LOG_GL_ERROR(glUniform1i(program->uniform.tex, 0));

	/* We require 1 byte alignment when uploading texture data */
	LOG_GL_ERROR(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

	/* Clamping to edges is important to prevent artifacts when scaling */
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

	/* Nearest filtering looks bettter for text.
	 *  The original code from wikibook used (Linear filtering) GL_LINEAR
	 *   that was drawing fuzzy text.
	 *
	 * Also,
	 * If GL_LINEAR is used, the fuzzy text can be partially solved by:
	 *  making text coord to  (same for y with surface-height)
	 *   `x += fmodf(fabs(x), 2.0 / surface-width);` */
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	LOG_GL_ERROR(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));

	LOG_GL_ERROR(glEnableVertexAttribArray(program->attribute.coord));

	/* Get glyph information and positions out of the buffer. */
	unsigned int i, len = hb_buffer_get_length(hb_buffer);
	hb_glyph_info_t *info = hb_buffer_get_glyph_infos(hb_buffer, NULL);
	//~ hb_glyph_position_t *gpos = hb_buffer_get_glyph_positions(hb_buffer, NULL);

	FT_GlyphSlot g = font->ft_face->glyph;

	/* Loop through all codepoint and draw them */
	for (i = 0; i < len; i++) {
		/* Try to load and render the character */
		if (FT_Load_Glyph(font->ft_face, info[i].codepoint, FT_LOAD_RENDER)) {
			LOG_WARN("FT_Load_Glyph failed for code point %i", info[i].codepoint);
			continue;
		}

		/* Upload the "bitmap", which contains an 8-bit grayscale image,
		 *  as an alpha texture */
		LOG_GL_ERROR(glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, g->bitmap.width,
			g->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer));

		/* Calculate the vertex and texture coordinates */
		float x2 = x + (g->metrics.horiBearingX / 64.) * sx;
		float y2 = y + (g->metrics.horiBearingY / 64.) * sy;

		/* these version have problem showing text */
		//~ float x2 = x + (gpos[i].x_offset / 64.) * sx;
		//~ float y2 = y + (gpos[i].y_offset / 64.) * sy;

		float w = g->bitmap.width * sx;
		float h = g->bitmap.rows * sy;

		lp_vec4f box[4] = {
			{x2, y2, 0, 0},
			{x2 + w, y2, 1, 0},
			{x2, y2 - h, 0, 1},
			{x2 + w, y2 - h, 1, 1},
		};

		if (rotate) {
			rotate_coords(box, 4, pos, rotate);
		}

		apply_translate(box, 4, translate);

		/* Draw the character on the screen */
		LOG_GL_ERROR(glVertexAttribPointer(program->attribute.coord,
			4, GL_FLOAT, false, 0, box));
		LOG_GL_ERROR(glDrawArrays(GL_TRIANGLE_STRIP, 0, 4));

		/* Advance the cursor to the start of the next character */
		x += (g->advance.x / 64.) * sx;
		y += (g->advance.y / 64.) * sy;

		/* these version have problem showing text */
		//~ x += (gpos[i].x_advance / 64.) * sx;
		//~ y += (gpos[i].y_advance / 64.) * sy;
	}

	LOG_GL_ERROR(glDisableVertexAttribArray(program->attribute.coord));
	LOG_GL_ERROR(glDeleteTextures(1, &tex));
}

static void prepare_char_size(lp_font *font, float height, lp_vec2f *dpi,
	float rotate)
{
	float dpi_x = dpi->x, dpi_y = dpi->y;
	if (rotate) {
		SWAP_VALUE(dpi_x, dpi_y, float)
	}

	if (FT_Set_Char_Size(font->ft_face, 0, height * 64, dpi_x, dpi_y)) {
		LOG_WARN("FT_Set_Char_Size failed");
	}
}

/* assumes that the buffer is shaped and
 *  the ft_font character size are already applied. */
void lp_program_text_draw_primitive(lp_program_text *program,
		hb_buffer_t *hb_buffer, lp_vec2f *pos, lp_surface *surface,
		lp_vec2f *dpi, lp_font *font, lp_color4f *color,
		float height, lp_align align, float rotate, lp_vec2f *original_translate)
{
	/* assure that the content is valid */
	hb_buffer_content_type_t content_type = hb_buffer_get_content_type(hb_buffer);
	LOG_ASSERT_RET_ON_FAIL(content_type != HB_BUFFER_CONTENT_TYPE_INVALID);

	/* assure we have a font */
	if (font == NULL) {
		font = lp_font_gen();
		LOG_ASSERT_RET_ON_FAIL(content_type == HB_BUFFER_CONTENT_TYPE_UNICODE);
	}

	prepare_char_size(font, height, dpi, rotate);

	if (content_type == HB_BUFFER_CONTENT_TYPE_UNICODE) {
		hb_shape(font->hb_font, hb_buffer, NULL, 0);
	}

	/* lock the font */
	lp_obj_manager_lock(&font->obj_manager);

	LOG_GL_ERROR(glUseProgram(program->id));
	LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1, CAST_TO_FLOAT_PTR(color)));

	lp_vec2f translate = *original_translate;

	if (align || rotate) {
		translate_from_aligned_and_rotate(hb_buffer, font, align, surface,
			rotate, &translate);
	}

	render_text(program, hb_buffer, font, &translate, pos, surface, rotate);

	/* release the font */
	lp_obj_manager_unlock(&font->obj_manager);
}

void lp_program_text_draw_text(lp_program_text *program,
		lp_text *text, hb_buffer_t *hb_buffer,
		lp_surface *surface, lp_vec2f *dpi, lp_vec2f *translate)
{
	LOG_ASSERT_RET_ON_NULL(text->string);

	hb_buffer_reset(hb_buffer);
	hb_buffer_add_utf8(hb_buffer, (const char *) text->string, -1, 0, -1);
	hb_buffer_guess_segment_properties(hb_buffer);

	lp_program_text_draw_primitive(program, hb_buffer,
		&text->position, surface, dpi, text->font, &text->color,
		text->height, text->align, text->rotate, translate);
}
