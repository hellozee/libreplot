/*
 * This file is part of libreplot
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "common.h"
#include <stdlib.h>

/*
 * Ref: https://gitlab.com/wikibooks-opengl/modern-tutorials
 */

static const char PROGRAM_VERSION_CODE_GLES[] =
	"#version 100\n";

static const char PROGRAM_VERSION_CODE_GL[] =
	"#version 120\n";

static const char PROGRAM_PRECISION_CODE_GLES[] =
	"#ifdef GL_FRAGMENT_PRECISION_HIGH\n"
	"precision highp float;           \n"
	"#else                            \n"
	"precision mediump float;         \n"
	"#endif                           \n";

static const char PROGRAM_PRECISION_CODE_GL[] =
	"#define lowp   \n"
	"#define mediump\n"
	"#define highp  \n";

/**
 * Compile the shader from file 'filename', with error handling
 */
unsigned lp_shader_create(lp_enum type, const char *source)
{
	const char *version_code =
		LP_TARGET_GLES ?
			PROGRAM_VERSION_CODE_GLES :
			PROGRAM_VERSION_CODE_GL;

	const char *precision_code = "";
	if (type == GL_FRAGMENT_SHADER) {
		precision_code =
			LP_TARGET_GLES ?
				PROGRAM_PRECISION_CODE_GLES :
				PROGRAM_PRECISION_CODE_GL;
	}

	const char *sources [3] = {
		version_code,
		precision_code,
		source
	};

	GLuint res = glCreateShader(type);
	glShaderSource(res, 3, sources, NULL);
	glCompileShader(res);
	GLint compile_ok = false;
	glGetShaderiv(res, GL_COMPILE_STATUS, &compile_ok);
	if (compile_ok == false) {
		lp_print_log(res);
		glDeleteShader(res);
		return 0;
	}

	return res;
}

GLuint lp_program_create(const char *vertexSource, const char *fragmentSource)
{
	GLuint program = glCreateProgram();
	GLuint shader;
	if (vertexSource) {
		shader = lp_shader_create(GL_VERTEX_SHADER, vertexSource);
		if (!shader) {
			return 0;
		}
		glAttachShader(program, shader);
	}

	if (fragmentSource) {
		shader = lp_shader_create(GL_FRAGMENT_SHADER, fragmentSource);
		if (!shader) {
			return 0;
		}
		glAttachShader(program, shader);
	}

	glLinkProgram(program);
	GLint link_ok = false;
	glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
	if (link_ok == false) {
		lp_print_log(program);
		glDeleteProgram(program);
		return 0;
	}

	return program;
}

GLint lp_program_attribute(GLuint program, const char *name)
{
	GLint attribute;
	LOG_GL_ERROR(attribute = glGetAttribLocation(program, name));

	if (attribute == -1) {
		LOG_WARN("Could not bind attribute %s", name);
	}

	return attribute;
}
GLint lp_program_uniform(GLuint program, const char *name)
{
	GLint uniform;
	LOG_GL_ERROR(uniform = glGetUniformLocation(program, name));

	if (uniform == -1) {
		LOG_WARN("Could not bind uniform %s", name);
	}

	return uniform;
}

/**
 * Display compilation errors from the OpenGL shader compiler
 */
void lp_print_log(GLuint object)
{
	GLint log_length = 0;
	if (glIsShader(object)) {
		glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
	} else if (glIsProgram(object)) {
		glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
	} else {
		LOG_WARN("Not a shader or a program\n");
		return;
	}

	char* log = (char *) malloc(log_length);
	if (glIsShader(object)) {
		glGetShaderInfoLog(object, log_length, NULL, log);
	} else if (glIsProgram(object)) {
		glGetProgramInfoLog(object, log_length, NULL, log);
	}

	LOG_WARN("%s", log);
	free(log);
}
