/*
 * This file is part of libreplot
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_PROGRAM_COMMON_H
#define LIBREPLOT_PROGRAM_COMMON_H

#include "../common.h"

__BEGIN_DECLS

LP_PRIV GLint lp_program_uniform(GLuint program, const char *name);
LP_PRIV GLint lp_program_attribute(GLuint program, const char *name);
LP_PRIV GLuint lp_program_create(const char *vertexSource, const char *fragmentSource);
LP_PRIV GLuint lp_shader_create(lp_enum type, const char *source);

LP_PRIV void lp_print_log(GLuint object);

__END_DECLS

#endif
