/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_PROGRAM_GENERIC_H
#define LIBREPLOT_PROGRAM_GENERIC_H

#include "common.h"
#include "../extra/line.h"
#include "../extra/rectangle.h"

__BEGIN_DECLS

struct lp_program_generic {
	GLuint id;
	struct { GLint color, scale, translate; } uniform;
	struct { GLint position; } attribute;
};

typedef struct lp_program_generic lp_program_generic;

LP_PRIV lp_program_generic *lp_program_generic_gen(lp_program_generic *program);
LP_PRIV void lp_program_generic_del(lp_program_generic *program);

LP_PRIV void lp_program_generic_draw_line_primitive(lp_program_generic *program,
					lp_vec2f *coords, size_t coords_count, lp_color4f *color,
					float width, lp_vec2f *scale, lp_vec2f *translate);

LP_PRIV void lp_program_generic_draw_line(lp_program_generic *program,
				lp_line *line, lp_vec2f *scale, lp_vec2f *translate);

LP_PRIV void lp_program_generic_draw_rectangle(lp_program_generic *program,
				lp_rectangle *rect, lp_vec2f *scale, lp_vec2f *translate);

__END_DECLS

#endif
