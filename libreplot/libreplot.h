/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_H
#define LIBREPLOT_H

#include "common.h"

#include "extra/font.h"
#include "extra/metric-prefix.h"
#include "extra/binary-prefix.h"
#include "extra/circle.h"
#include "extra/line.h"
#include "extra/text.h"
#include "extra/texture.h"

#include "area/area.h"

#include "cartesian/axis.h"
#include "cartesian/curve.h"
#include "cartesian/cartesian.h"

#endif
