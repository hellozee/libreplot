/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_CARTESIAN_AXIS_H
#define LIBREPLOT_CARTESIAN_AXIS_H

#include "../common.h"
#include "../extra/metric-prefix.h"
#include "../extra/binary-prefix.h"
#include "../extra/line.h"
#include "../extra/text.h"
#include "../extra/handle.h"

__BEGIN_DECLS

/* cartesian_axis should assigned to one cartesian only.
 *  and it should be alive till to-assigned is alive.
 * This do not perform reference counting. and is not thread safe. */

typedef struct lp_cartesian_axis lp_cartesian_axis;

LP_API lp_cartesian_axis *lp_cartesian_axis_gen(void);
LP_API void lp_cartesian_axis_ref(lp_cartesian_axis *cartesian_axis);
LP_API void lp_cartesian_axis_del(lp_cartesian_axis *cartesian_axis);

enum lp_cartesian_axis_prefix_select {
	LP_CARTESIAN_AXIS_PREFIX_SELECT_NONE = 0,
	LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC = 1,
	LP_CARTESIAN_AXIS_PREFIX_SELECT_BINARY = 2
};

enum lp_cartesian_axis_scale_select {
	LP_CARTESIAN_AXIS_SCALE_SELECT_LINEAR = 0,
	LP_CARTESIAN_AXIS_SCALE_SELECT_LOG = 1
};

typedef enum lp_cartesian_axis_scale_select lp_cartesian_axis_scale_select;
typedef enum lp_cartesian_axis_prefix_select lp_cartesian_axis_prefix_select;

enum lp_cartesian_axis_part {
	/* boolean */
	LP_CARTESIAN_AXIS_NAME_SHOW = 0,
	LP_CARTESIAN_AXIS_BASELINE_SHOW = 1,
	LP_CARTESIAN_AXIS_TICK_SHOW = 2,
	LP_CARTESIAN_AXIS_VALUE_SHOW = 3,
	LP_CARTESIAN_AXIS_GRID_SHOW = 4,

	/* float */
	LP_CARTESIAN_AXIS_NAME_HEIGHT = 10,
	LP_CARTESIAN_AXIS_VALUE_HEIGHT = 11,
	LP_CARTESIAN_AXIS_BASELINE_WIDTH = 12,
	LP_CARTESIAN_AXIS_TICK_WIDTH = 13,
	LP_CARTESIAN_AXIS_TICK_LENGTH = 14,
	LP_CARTESIAN_AXIS_DIV_AUTO_SPACING_HINT = 15, /* if div=auto is set, this value
	   user set the value of space division (unit: mm).
	   note: this is a hint only, can be ignore */

	/* pointer: string */
	LP_CARTESIAN_AXIS_NAME_TEXT = 20,
	LP_CARTESIAN_AXIS_VALUE_APPEND = 21, /* usecase: unit */
	LP_CARTESIAN_AXIS_VALUE_PREPEND = 22, /* usecase: $ sign */

	/* 4float */
	LP_CARTESIAN_AXIS_NAME_COLOR = 30,
	LP_CARTESIAN_AXIS_BASELINE_COLOR = 31,
	LP_CARTESIAN_AXIS_TICK_COLOR = 32,
	LP_CARTESIAN_AXIS_VALUE_COLOR = 33,
	LP_CARTESIAN_AXIS_GRID_COLOR = 34,

	/* pointer: font */
	LP_CARTESIAN_AXIS_NAME_FONT = 40,
	LP_CARTESIAN_AXIS_VALUE_FONT = 41,

	/* enum: lp_cartesian_axis_scale_select */
	LP_CARTESIAN_AXIS_SCALE_SELECT = 50,

	/* enum: lp_cartesian_axis_prefix */
	LP_CARTESIAN_AXIS_PREFIX_SELECT = 60,

	/* pointer: lp_metric_prefix * */
	LP_CARTESIAN_AXIS_PREFIX_METRIC = 70,

	/* pointer: lp_binary_prefix * */
	LP_CARTESIAN_AXIS_PREFIX_BINARY = 80,

	/* uint */
	LP_CARTESIAN_AXIS_DIV_COUNT_MAX = 90, /**< Maximum number of
		ticks/values/grid to show. (0 = auto) */

	/* 2float */
	LP_CARTESIAN_AXIS_VALUE_RANGE = 100, /* min, max */
};

typedef enum lp_cartesian_axis_part lp_cartesian_axis_part;

LP_API void lp_cartesian_axis_bool(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, bool arg0);

LP_API void lp_cartesian_axis_float(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, float arg0);

LP_API void lp_cartesian_axis_2float(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, float arg0, float arg1);

LP_API void lp_get_cartesian_axis_2float(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, float *arg0, float *arg1);

LP_API void lp_cartesian_axis_4float(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part,
			float arg0, float arg1, float arg2, float arg3);

LP_API void lp_cartesian_axis_enum(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, lp_enum arg0);

LP_API void lp_cartesian_axis_uint(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, unsigned arg0);

LP_API void lp_cartesian_axis_pointer(lp_cartesian_axis *cartesian_axis,
			lp_cartesian_axis_part part, void *arg0);

/**
 * =========================
 * warning: NOT implemented
 * =========================
 *
 * Set axis values.                                                           \n
 * usecase 1:                                                                 \n
 *  show numbers dynamically.                                                 \n
 *   set @a names = NULL                                                      \n
 *   set @a position = NULL                                                   \n
 *   set @a count = 0                                                         \n
 *  generated: dynamic nice ticks (as you expect from any plotting library)   \n
 *                                                                            \n
 * usecase 2:                                                                 \n
 *  show special text only at positions 0, 1, 2, 3 ... N                      \n
 *   set @a names = special text                                              \n
 *   set @a positions = NULL                                                  \n
 *   set @a count = N                                                         \n
 *  generated:                                                                \n
 *   for (i = 0; i < @a count; i++) {                                         \n
 *       show text @a names [i] at position i                                 \n
 *   }                                                                        \n
 *                                                                            \n
 * usecase 3:                                                                 \n
 *  show special N text only at special N positions.                          \n
 *   set @a names = special text                                              \n
 *   set @a positions = special positions                                     \n
 *   set @a count = N                                                         \n
 *  generated:                                                                \n
 *   for (i = 0; i < @a count; i++) {                                         \n
 *       show @a names [i] at position positions [i]                          \n
 *   }                                                                        \n
 *                                                                            \n
 * usecase 4:                                                                 \n
 *  show special values at N positions                                        \n
 *   set @a names = NULL                                                      \n
 *   set @a positions = special values                                        \n
 *   set @a count = N                                                         \n
 *  generated:                                                                \n
 *   for (i = 0; i < @a count; i++) {                                         \n
 *       show stringify( @a positions [i]) at position positions [i]          \n
 *   }                                                                        \n
 *                                                                            \n
 * usecase 5:                                                                 \n
 *  show  special values at positions 0, 1, 2, ... N                          \n
 *   set @a names = NULL                                                      \n
 *   set @a positions = special values                                        \n
 *   set @a count = N                                                         \n
 *  generated:                                                                \n
 *   for (i = 0; i < @a count; i++) {                                         \n
 *       show stringify(i) at position i                                      \n
 *   }                                                                        \n
 *
 * @note all values in @a positions should be in increment order
 *
 * @param cartesian_axis Axis
 * @param names Names (can be NULL)
 * @param positions Positions (can be NULL)
 * @param count Number of items in @a names (if not NULL) and @a positions (if not NULL)
 * @warning @a names and @a positions should be valid till used.
 *   (ie until the axis object is deleted OR another call to this function replace it)
 */
LP_API void lp_cartesian_axis_values(lp_cartesian_axis *cartesian_axis,
	uint8_t **names, float *positions, size_t count);

__END_DECLS

#endif
