/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_CARTESAIN_AXIS_PRIVATE_H
#define LIBREPLOT_CARTESAIN_AXIS_PRIVATE_H

#include "../common.h"
#include "axis.h"
#include "../extra/obj-manager-private.h"

__BEGIN_DECLS

struct lp_cartesian_axis {
	lp_obj_manager obj_manager;

	struct {
		bool show;
		lp_font *font;
		float height;
		uint8_t *value;
		lp_color4f color;
	} name;

	struct {
		bool show;
		lp_handle *design;
	} handle;

	/* the line that divide data and text */
	struct {
		bool show;
		lp_color4f color;
		float width;
	} baseline;

	/* lines that are shown in background of data */
	struct {
		bool show;
		lp_color4f color;
		float width;
	} grid;

	/* the perpendicular tickmark draw on "line" */
	struct {
		bool show;
		lp_color4f color;
		float width;
		float length;
	} tick;

	/* value of the tickmark */
	struct {
		bool show;
		float height;
		lp_color4f color;
		lp_font *font;

		uint8_t *append, *prepend;

		/* provided by values() function */
		struct {
			uint8_t **names;
			float *positions;
			size_t count;
		} user;

		float min, max;
	} value;

	/* prefix selection */
	struct {
		lp_cartesian_axis_prefix_select select;
		lp_metric_prefix *metric;
		lp_binary_prefix *binary;
	} prefix;

	/* if NULL, there is no unit */
	struct {
		bool show;
		uint8_t *value;
	} unit;

	struct {
		lp_cartesian_axis_scale_select scale;
		unsigned count;
		float auto_spacing_hint; /* unit: mm */
	} div;
};

__END_DECLS

#endif
