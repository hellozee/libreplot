/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_CARTESAIN_H
#define LIBREPLOT_CARTESAIN_H

#include "../common.h"

#include "axis.h"
#include "curve.h"
#include "../extra/circle.h"
#include "../extra/line.h"
#include "../extra/text.h"
#include "../extra/rectangle.h"

__BEGIN_DECLS

typedef struct lp_cartesian lp_cartesian;

/**
 * Assuming:
 * - Scissor is enabled [use `glEnable(GL_SCISSOR_TEST)`]
 */

LP_API lp_cartesian *lp_cartesian_gen(void);
LP_API void lp_cartesian_ref(lp_cartesian *cartesian);
LP_API void lp_cartesian_del(lp_cartesian *cartesian);

enum lp_cartesian_part {
	/* 4uint */
	LP_CARTESIAN_SURFACE = 0, /* x, y, width, height */

	/* 2float */
	LP_CARTESIAN_DPI = 10, /* x, y */

	/* pointer: lp_cartesian_axis */
	LP_CARTESIAN_AXIS_AT_BOTTOM = 20,
	LP_CARTESIAN_AXIS_AT_LEFT = 21,
	LP_CARTESIAN_AXIS_AT_TOP = 22,
	LP_CARTESIAN_AXIS_AT_RIGHT = 23

	/* float */
	/* LP_CARTESIAN_CIRCLE_APPROX_ARC = 30,  future */
};

typedef enum lp_cartesian_part lp_cartesian_part;

LP_API void lp_cartesian_4uint(lp_cartesian *cartesian, lp_cartesian_part part,
	unsigned arg0, unsigned arg1, unsigned arg2, unsigned arg3);

LP_API void lp_cartesian_2float(lp_cartesian *cartesian,
	lp_cartesian_part part, float arg0, float arg1);

LP_API void lp_cartesian_pointer(lp_cartesian *cartesian,
	lp_cartesian_part part, void *arg0);

LP_API void lp_cartesian_draw_start(lp_cartesian *cartesian);

LP_API void lp_cartesian_draw_curve(lp_cartesian *cartesian, lp_cartesian_curve *curve);
LP_API void lp_cartesian_draw_circle(lp_cartesian *cartesian, lp_circle *circle);
LP_API void lp_cartesian_draw_line(lp_cartesian *cartesian, lp_line *line);
LP_API void lp_cartesian_draw_text(lp_cartesian *cartesian, lp_text *text);
LP_API void lp_cartesian_draw_rectangle(lp_cartesian *cartesian, lp_rectangle *rect);

LP_API void lp_cartesian_draw_end(lp_cartesian *cartesian);

LP_API void lp_cartesian_renew(lp_cartesian *cartesian, unsigned tag);

LP_API void lp_cartesian_pixel_to_data_coord(lp_cartesian *cartesian,
	int px, int py, float *cx, float *cy);

__END_DECLS

#endif
