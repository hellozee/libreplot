# - Try to find GLESv2
#
#  GLESv2_FOUND - system has GLESv2
#  GLESv2_INCLUDE_DIRS - the GLESv2 include directory
#  GLESv2_LIBRARIES - Link these to use GLESv2
#  GLESv2_DEFINITIONS - Compiler switches required for using GLESv2
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(GLESv2_INCLUDE_DIR
	NAMES GLES2/gl2.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "GLESv2.so"
FIND_LIBRARY(GLESv2_LIBRARY
	NAMES GLESv2 libGLESv2
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (GLESv2_INCLUDE_DIR AND GLESv2_LIBRARY)
	SET(GLESv2_FOUND TRUE)
	SET(GLESv2_INCLUDE_DIRS ${GLESv2_INCLUDE_DIR})
	SET(GLESv2_LIBRARIES ${GLESv2_LIBRARY})
ENDIF (GLESv2_INCLUDE_DIR AND GLESv2_LIBRARY)

IF (GLESv2_FOUND)
	IF (NOT GLESv2_FIND_QUIETLY)
		MESSAGE(STATUS "Found GLESv2:")
		MESSAGE(STATUS " - Includes: ${GLESv2_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${GLESv2_LIBRARIES}")
	ENDIF (NOT GLESv2_FIND_QUIETLY)
ELSE (GLESv2_FOUND)
	IF (GLESv2_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find GLESv2")
	ENDIF (GLESv2_FIND_REQUIRED)
ENDIF (GLESv2_FOUND)

# show the GLESv2_INCLUDE_DIRS and GLESv2_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(GLESv2_INCLUDE_DIRS GLESv2_LIBRARIES)
