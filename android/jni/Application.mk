#
# This file is part of libreplot.
#
# Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# libreplot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libreplot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
#

APP_ABI:= all
APP_LDLIBS:= -llog -lGLESv2
APP_PLATFORM := android-8
