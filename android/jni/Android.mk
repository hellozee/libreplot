#
# This file is part of libreplot.
#
# Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# libreplot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libreplot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

REL_PATH:= ../..
ABS_PATH:= $(LOCAL_PATH)/$(REL_PATH)

define all-c-files-under
	$(patsubst ./%, %, 									\
		$(shell cd $(LOCAL_PATH) ;						\
			find $(1) -name "*.c" -and -not -name ".*") \
		)
endef

LOCAL_SRC_FILES:= $(call all-c-files-under, $(REL_PATH)/libreplot)

LOCAL_C_INCLUDES:= $(ABS_PATH)/libreplot
LOCAL_EXPORT_C_INCLUDES:= $(ABS_PATH)

LOCAL_MODULE:= replot
LOCAL_LDLIBS:= -llog -lGLESv2
LOCAL_SHARED_LIBRARIES:= png freetype2 harfbuzz fontconfig

LOCAL_EXPORT_CFLAGS:= -DLP_USE_GLES2=1

LOCAL_CFLAGS += -DLP_USE_GLES2=1
LOCAL_CFLAGS += -DLP_FONT_DEFAULT_PATH=\"/system/fonts/DroidSans.ttf\"

ifeq ($(NDK_DEBUG),"1")
# debug build
LOCAL_CFLAGS += -g -Wall -Wwrite-strings -Wsign-compare
LOCAL_CFLAGS += -Wextra  -Wno-empty-body -Wno-unused-parameter
LOCAL_CFLAGS += -Wformat-security
endif

include $(BUILD_SHARED_LIBRARY)

# provide ndk-build LIBGPNG_PATH=<path-to-libpng-code>
# and same for FREETYPE2_PATH, HARFBUZZ_PATH, FONTCONFIG_PATH

import-it = 										\
	$(call import-add-path,$(shell dirname $(1)));	\
	$(call import-module,$(shell basename $(1))/$(2))

$(call import-it,$(LIBPNG_PATH),android/jni)
$(call import-it,$(FREETYPE2_PATH),builds/android/jni)
$(call import-it,$(HARFBUZZ_PATH),android/jni)
